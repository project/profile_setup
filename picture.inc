<?php

/**
 * contains all elements related to setting up a profile picture
 */
/**
 * default step 1 - upload a profile picture
 */
function _profile_setup_picture_form($category) {
  global $user;
  $edit = $user;

  //skip to next step
  if ($_POST['op'] == t('Skip this') || !variable_get('profile_setup_user_pictures', 0)) {
    $_SESSION['profile_setup']['step'] = $_SESSION['profile_setup']['step'] + 1;
    drupal_goto(_profile_setup_url($user));
  }

  //if no categories session create one
  _profile_setup_create_categories_session($user);
  $_SESSION['profile_setup']['step'] = _category_step($category);

  // Picture/avatar:
  if (variable_get('profile_setup_user_pictures', 0)) {
    $form['picture'] = array(
      '#type' => 'fieldset',
      '#title' => t('Your Profile Picture'),
      '#weight' => 1,
    );
    $picture = theme('user_picture', (object)$edit);
    if ($picture) {
      //$form['picture']['current_picture'] = array('#value' => $picture);
      $form['picture']['picture_delete'] = array('#type' => 'hidden');
    }
    else {
      $form['picture']['picture_delete'] = array('#type' => 'hidden');
    }
    $form['picture']['picture_upload'] = array(
      '#type' => 'file',
      '#title' => t('Upload a profile picture'),
      '#size' => 48,
      '#description' => t('Your virtual face or picture. Maximum dimensions are %dimensions and the maximum size is %size kB.', array('%dimensions' => variable_get('user_picture_dimensions', '85x85'), '%size' => variable_get('user_picture_file_size', '30'))) .' '. variable_get('user_picture_guidelines', '')
    );
  }
  $form['category'] = array(
    '#type' => 'hidden',
    '#value' => $category,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 30,
    '#prefix' => '<div class="loader"></div><div class="buttons">'
  );
  $form['skip'] = array(
    '#type' => 'submit',
    '#value' => t('Skip this'),
    '#weight' => 40,
    '#prefix' => ' | ',
    '#suffix' => '</div>'
  );
  $form['#attributes'] = array('enctype' => 'multipart/form-data');
  return $form;
}

/**
 * validation
 */
function _profile_setup_validate_picture($file) {
  global $user;

  // Check that uploaded file is an image, with a maximum file size
  // and maximum height/width.
  $info = image_get_info($file->filepath);
  list($maxwidth, $maxheight) = explode('x', variable_get('user_picture_dimensions', '85x85'));

  if (!$info || !$info['extension']) {
    form_set_error('picture_upload', t('The uploaded file was not an image.'));
    return FALSE;
  }
  else if (image_get_toolkit()) {
    image_scale($file->filepath, $file->filepath, $maxwidth, $maxheight);
  }
  else if (filesize($file->filepath) > (variable_get('user_picture_file_size', '30') * 1000)) {
    form_set_error('picture_upload', t('The uploaded image is too large; the maximum file size is %size kB.', array('%size' => variable_get('user_picture_file_size', '30'))));
    return FALSE;
  }
  else if ($info['width'] > $maxwidth || $info['height'] > $maxheight) {
    form_set_error('picture_upload', t('The uploaded image is too large; the maximum dimensions are %dimensions pixels.', array('%dimensions' => variable_get('user_picture_dimensions', '85x85'))));
    return FALSE;
  }

  //Rename picture with unique id - take from unique_avatar.module
  $dest = variable_get('user_picture_path', 'pictures') .'/picture-'. $user->uid .'-'. md5(time()) .'.'. $info['extension'];
  if (file_move($file->filepath, $dest)) {

    //Clean up existing picture
    if (module_exists('imagecache')) {
      imagecache_image_flush($user->picture);
    }
    file_delete($user->picture);

    //Assign new picture
    $edit['picture'] = file_directory_path() .'/'. $dest;
    $user->picture = file_directory_path() .'/'. $dest;
    db_query("UPDATE {users} SET picture = '%s' WHERE uid = %d", $user->picture, $user->uid);
    return TRUE;
  }
  return FALSE;
}

/** 
 * file functions
 * 1) add ajax loaders to upload, preview and submit buttons
 * 2) finds form errors and prevents form submit if errors
 */
var ProjectsSetup = ProjectsSetup || {};

ProjectsSetup.initialize = function() {
  //settings loaded from page vars
  var basePath = Drupal.settings.ProjectsSetup.basePaths['base'];
  var modulePath = Drupal.settings.ProjectsSetup.basePaths['module'];	
  var loaderPath = basePath + Drupal.settings.ProjectsSetup.basePaths['loader'];  
	preloadLoader(loaderPath);
	$('input[@value=Save]#edit-save, input[@value=Save]#edit-submit').bind('click', saveClick);
	$('input[@value=Skip this]#edit-skip').bind('click', skipClick);
};

saveClick = function() {
  errors = false;
	var i = 0;
	var first;
	$("form#profile-setup-form").find('input[@type=text], textarea, select').each(
    function() {		  			
			if($(this).is(".required")){				
				if (this.value == '') {					
					$(this).addClass('error');
					errors = true;
					++i;
					if (i == 1) {
						first = $(this);
					}					
				}
			}
		}
	);	
	if (errors) {
    alert('Some required fields (highlighted in red) have been left blank!');
    first.focus();
		return false;
	}

	addLoader($('input[@value=Save]#edit-save'), 'Saving....', '');	
	return true;
};

skipClick = function() {
	addLoader($(this), '', '');	
	return true;
};

addLoader = function(parent, text, before) {
  //settings loaded from page vars
  var basePath = Drupal.settings.ProjectsSetup.basePaths['base'];	
  var loaderPath = basePath + Drupal.settings.ProjectsSetup.basePaths['loader'];   
	$('.buttons').hide();
	$('.loader').html(before +'<img src="' + loaderPath + '" align="absmiddle" />&nbsp;<strong>'+ text +'</strong>')
};

preloadLoader = function(path) {
  preloaded = document.createElement('img');
  preloaded.setAttribute('src',path);	
};

if (Drupal.jsEnabled) {
  $(document).ready(function() {	
		ProjectsSetup.initialize();
  });
}

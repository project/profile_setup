<?php

/**
 * contains all worker functions
 */
/**
 * generic form hook
 * TODO
 * - allow selection of specific fields rather than a whole category?
 */
function _profile_setup_form($category) {
  global $user, $_birthdays_field;
  $user = user_load(array('uid' => $user->uid));
  $edit = (array)$user;

  //skip to next step
  if ($_POST['op'] == t('Skip this')) {
    $_SESSION['profile_setup']['step'] = $_SESSION['profile_setup']['step'] + 1;
    drupal_goto(_profile_setup_url($user));
  }
  $_SESSION['profile_setup']['step'] = _category_step($category);

  $form = profile_form_profile($edit, $user, $category);
  $form['category'] = array(
    '#type' => 'hidden',
    '#value' => $category,
  );
  $form['_account'] = array(
    '#type' => 'value',
    '#value' => $user,
  );
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 30,
    '#prefix' => '<div class="loader"></div><div class="buttons">'
  );
  $form['skip'] = array(
    '#type' => 'submit',
    '#value' => t('Skip this'),
    '#weight' => 40,
    '#prefix' => ' | ',
    '#suffix' => '</div>'
  );

  //add birthday module integration
  if ($_birthdays_field) {
    if ($form[$_birthdays_field->category][$_birthdays_field->name]) {
      $form[$_birthdays_field->category][$_birthdays_field->name]['#process'] = array('expand_birthdays_date' => array());
      $form[$_birthdays_field->category][$_birthdays_field->name]['#validate'] = array('birthdays_date_validate' => array());
    }
  }
  return $form;
}

function __profile_setup_url($account) {
  $category = $_SESSION['profile_setup']['profile_categories'];
  $total_steps = count($category);
  if ($_SESSION['profile_setup']['step'] >= $total_steps) {
		//last step - destination - session set URL take priority - default is "user"
		$node_replace_vars = array();
		$node_replace_vars['!uid'] = $account->uid;
		$node_replace_vars['!username'] = $account->name;
    $redirect = t(variable_get('profile_setup_oncomplete_redirect', 'user'), $node_replace_vars);
    $sessionredirect = $_SESSION['profile_setup']['destination'];		
		$dest = !empty($redirect) ? $redirect : 'user';
		$dest = !empty($sessionredirect) ? $sessionredirect : $dest;
    _clean_profile_setup_sessions();
    drupal_set_message(t('Thank you for setting up your profile'));
    //module_invoke_all('profile_setup', 'complete', $account, $dest); //API - now using workflow_ng
    if (module_exists('workflow_ng')) {
		  workflow_ng_invoke_event('profile_setup_complete', $account); //workflow interaction
		}
    return $dest;
  }

  return 'profilesetup/'. $category[$_SESSION['profile_setup']['step']]['title'];
}

/**
 * returns all the categories from the profile modules
 * Step 1 is always picture as affects logic elsewhere in module
 */
function __get_profile_categories($exclude = array(), $account = NULL, $sort = TRUE) {
  $step = 0;
  $categories = FALSE;

  //profile picture
  if (variable_get('profile_setup_user_pictures', 0)) {
    $categories[$step] = array(
      'title' => 'picture',
      'weight' => _step_order_value('picture', 'picture', $step),
      'name' => 'picture',
      );
    ++$step;
  }

  //change username step
  if (variable_get('profile_setup_change_username', 0) && user_access('change own username', $account)) {
    $categories[$step] = array(
      'title' => 'username',
      'weight' => _step_order_value('username', 'username', $step),
      'name' => 'username',
      );
    ++$step;
  }
  
	if (db_table_exists('profile_fields')) {//module can be run without 
    $result = db_query("SELECT DISTINCT(category) FROM {profile_fields} ORDER BY category");
    while ($cat = db_fetch_object($result)) {
      //TODO - check fields in each cat are filled only show cat if empty
      if (!in_array($cat->category, $exclude)) {
        $cfn = _profile_setup_computer_friendly_name($cat->category);
        $categories[$step] = array(
          'title' => $cat->category,
          'weight' => _step_order_value($cfn, $cat->category, $step),
          'name' => $cfn,
        );
        ++$step;
      }
    }
	}

  if ($categories) {
    if ($sort) {
      usort($categories, '_profile_setup_sort');
    }
    foreach ($categories as $k => $category) {
      $_SESSION['profile_setup']['steps'][$category['title']] = $k;
    }
  }

  return $categories;
}
